unit Unit4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids;

type
  TForm4 = class(TForm)
    strngrd1: TStringGrid;
    cbb1: TComboBox;
    lbledt1: TLabeledEdit;
    btn1: TButton;
    btn2: TButton;
    lbl1: TLabel;
    btn3: TButton;
    procedure FormShow(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;
  selected_group_id_tbl1: Integer;

implementation

uses Unit1;

{$R *.dfm}


procedure TForm4.FormShow(Sender: TObject);

var
  i: Integer;

begin
  strngrd1.ColWidths[0]:=125;
  strngrd1.ColWidths[1]:=125;
  strngrd1.ColWidths[2]:=100;
  strngrd1.ColWidths[3]:=110;
  strngrd1.ColWidths[4]:=170;
  strngrd1.Cells[0,0]:='����� ���������';
  strngrd1.Cells[1,0]:='�������� ���������';
  strngrd1.Cells[2,0]:='����� (�������)';
  strngrd1.Cells[3,0]:='���� ��������';
  strngrd1.Cells[4,0]:='����������';
  strngrd1.RowCount:=2;

  MainForm.tbl1.First;
  for i:=0 to (MainForm.tbl1.RecordCount-1)
    do
      begin
        //Form3.cbb1.Items.Add(MainForm.tbl1.Fields.Fields[i].AsString);
        Form4.cbb1.Items.Add(MainForm.tbl1.FieldByName('Group_numbers').AsString);
        MainForm.tbl1.Next;
      end;
  Form4.cbb1.Update;
end;

procedure TForm4.btn2Click(Sender: TObject);
begin
  Form4.Close;
end;


procedure TForm4.btn1Click(Sender: TObject);

var
  i: Integer;

begin
  for i:=strngrd1.RowCount-1  downto  strngrd1.FixedRows+1
   do
    begin
      strngrd1.Rows[i].Clear;
      strngrd1.RowCount:=strngrd1.RowCount-1;
    end;
  strngrd1.Rows[1].Clear;
  if (cbb1.ItemIndex=-1) //or (lbledt1.Text='')
    then
      begin
        ShowMessage('�� ������� ������');//��� ��� ������� ---> (lbledt1.Text='')
        Exit;
      end;
  MainForm.tbl1.First;
  while (MainForm.tbl1.FieldByName('Group_numbers').AsString<>cbb1.Items.Strings[cbb1.ItemIndex]) and (not MainForm.tbl1.Eof)
    do
      MainForm.tbl1.Next;
  selected_group_id_tbl1:=MainForm.tbl1.RecNo;//.FieldByName('Group_id').AsInteger;
  MainForm.tbl2.First;
  while not MainForm.tbl2.Eof
    do
      begin
        if (MainForm.tbl1.FieldByName('Group_id').AsInteger=MainForm.tbl2.FieldByName('Group_id').AsInteger) and ((Pos(AnsiUpperCase(lbledt1.Text), AnsiUpperCase(MainForm.tbl2.FieldByName('Document_name').AsString))<>0) or (Pos(AnsiUpperCase(lbledt1.Text), AnsiUpperCase(MainForm.tbl2.FieldByName('Document_number').AsString))<>0))
          then
            begin
              strngrd1.Cells[0, strngrd1.RowCount-1]:=MainForm.tbl2.FieldByName('Document_number').AsString;
              strngrd1.Cells[1, strngrd1.RowCount-1]:=MainForm.tbl2.FieldByName('Document_name').AsString;
              strngrd1.Cells[2, strngrd1.RowCount-1]:=MainForm.tbl2.FieldByName('Author').AsString;
              strngrd1.Cells[3, strngrd1.RowCount-1]:=MainForm.tbl2.FieldByName('Created').AsString;
              strngrd1.Cells[4, strngrd1.RowCount-1]:=MainForm.tbl2.FieldByName('Note').AsString;
              //Inc(strngrd1.RowCount);
              strngrd1.RowCount:=strngrd1.RowCount+1;
            end;
        MainForm.tbl2.Next;
      end;

end;

procedure TForm4.btn3Click(Sender: TObject);

var
  i: Integer;

begin
  MainForm.tbl2.First;
  i:=1;
  while (not MainForm.tbl2.Eof) and (MainForm.tbl2.FieldByName('Document_number').AsString<>strngrd1.Cells[0,strngrd1.Row]) and (MainForm.tbl2.FieldByName('Group_id').AsInteger<>selected_group_id_tbl1)
    do
      begin
        MainForm.tbl2.Next;
        inc(i);
      end;
  MainForm.tbl1.First;
  while (not MainForm.tbl1.Eof) and (MainForm.tbl1.RecNo<>selected_group_id_tbl1)
    do
      begin
        MainForm.tbl1.Next;
      end;
  while (not MainForm.tbl2.Eof) and (MainForm.tbl2.RecNo<>i)
    do
      begin
        MainForm.tbl2.Next;
      end;
  id1:=MainForm.tbl1.FieldByName('Group_id').AsInteger;
  id2:=MainForm.tbl2.FieldByName('id').AsInteger;
  Form4.Close;
end;

end.
