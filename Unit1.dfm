object MainForm: TMainForm
  Left = 689
  Top = 200
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1056#1077#1076#1072#1082#1090#1086#1088' '#1073#1072#1079' '#1076#1072#1085#1085#1099#1093' '#1050#1044
  ClientHeight = 563
  ClientWidth = 892
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = mm1
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dbgrd1: TDBGrid
    Left = 12
    Top = 8
    Width = 462
    Height = 120
    DataSource = ds1
    Options = [dgEditing, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnCellClick = dbgrd1CellClick
  end
  object dbgrd2: TDBGrid
    Left = 8
    Top = 139
    Width = 792
    Height = 399
    DataSource = ds2
    Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    PopupMenu = PopupMenu1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnCellClick = dbgrd2CellClick
    OnMouseWheel = dbgrd2MouseWheel
  end
  object btn1: TButton
    Left = 480
    Top = 10
    Width = 90
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 2
    OnClick = btn1Click
  end
  object btn2: TButton
    Left = 480
    Top = 45
    Width = 90
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 3
    OnClick = btn2Click
  end
  object btn3: TButton
    Left = 812
    Top = 140
    Width = 75
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100'...'
    TabOrder = 4
    OnClick = btn3Click
  end
  object btn7: TButton
    Left = 812
    Top = 175
    Width = 75
    Height = 25
    Caption = #1053#1072#1081#1090#1080'...'
    TabOrder = 5
    OnClick = btn7Click
  end
  object stat1: TStatusBar
    Left = 0
    Top = 544
    Width = 892
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object mm1: TMainMenu
    Left = 744
    Top = 8
    object N1: TMenuItem
      Caption = #1060#1072#1081#1083
      object N3: TMenuItem
        Caption = #1054#1090#1082#1088#1099#1090#1100' '#1041#1044'...'
        ShortCut = 16463
        OnClick = N3Click
      end
      object N6: TMenuItem
        Caption = #1056#1077#1078#1080#1084
        Enabled = False
        object N12: TMenuItem
          Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1103
          RadioItem = True
          OnClick = N12Click
        end
        object N13: TMenuItem
          Caption = #1063#1090#1077#1085#1080#1103
          RadioItem = True
          OnClick = N13Click
        end
      end
      object N4: TMenuItem
        Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
        Enabled = False
        OnClick = N4Click
      end
      object N7: TMenuItem
        Caption = #1042#1099#1093#1086#1076
        OnClick = N7Click
      end
    end
    object N2: TMenuItem
      Caption = #1055#1086#1084#1086#1097#1100
      object N5: TMenuItem
        Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
        OnClick = N5Click
      end
    end
  end
  object ds1: TDataSource
    DataSet = tbl1
    Left = 232
    Top = 48
  end
  object tbl1: TADOTable
    Connection = con1
    CursorType = ctStatic
    AfterScroll = tbl1AfterScroll
    TableName = 'Groups'
    Left = 312
    Top = 48
    object atncfldtbl1Group_id: TAutoIncField
      DisplayWidth = 10
      FieldName = 'Group_id'
      ReadOnly = True
      Visible = False
    end
    object wdstrngfldtbl1Group_numbers: TWideStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
      DisplayWidth = 25
      FieldName = 'Group_numbers'
      Size = 255
    end
    object wdstrngfldtbl1Description: TWideStringField
      DisplayLabel = #1054#1087#1080#1089#1072#1085#1080#1077
      DisplayWidth = 45
      FieldName = 'Description'
      Size = 50
    end
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 48
    Top = 216
    object N8: TMenuItem
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1095#1077#1088#1090#1077#1078
      OnClick = N8Click
    end
    object N9: TMenuItem
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1084#1086#1076#1077#1083#1100
      OnClick = N9Click
    end
    object N10: TMenuItem
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      OnClick = N10Click
    end
    object N11: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnClick = N11Click
    end
  end
  object tbl2: TADOTable
    Connection = con1
    CursorType = ctStatic
    IndexFieldNames = 'Group_id'
    MasterFields = 'Group_id'
    MasterSource = ds1
    TableName = 'Documentation'
    Left = 384
    Top = 336
    object wdstrngfldtbl2Document_number: TWideStringField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      DisplayWidth = 25
      FieldName = 'Document_number'
      Size = 255
    end
    object wdstrngfldtbl2Document_name: TWideStringField
      DisplayLabel = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
      DisplayWidth = 34
      FieldName = 'Document_name'
      Size = 255
    end
    object wdstrngfldtbl2Author: TWideStringField
      DisplayLabel = #1040#1074#1090#1086#1088
      DisplayWidth = 15
      FieldName = 'Author'
      Size = 255
    end
    object wdstrngfldtbl2Created: TWideStringField
      DisplayLabel = #1044#1072#1090#1072' '#1089#1086#1079#1076#1072#1085#1080#1103
      DisplayWidth = 18
      FieldName = 'Created'
      Size = 255
    end
    object wdstrngfldtbl2Note: TWideStringField
      DisplayLabel = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
      DisplayWidth = 30
      FieldName = 'Note'
      Size = 255
    end
    object atncfldtbl2id: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
      Visible = False
    end
    object intgrfldtbl2Group_id: TIntegerField
      FieldName = 'Group_id'
      Visible = False
    end
    object mfldtbl2File_path: TMemoField
      FieldName = 'File_path'
      Visible = False
      BlobType = ftMemo
    end
    object mfldtbl2Model_path: TMemoField
      FieldName = 'Model_path'
      Visible = False
      BlobType = ftMemo
    end
  end
  object ds2: TDataSource
    DataSet = tbl2
    Left = 240
    Top = 336
  end
  object con1: TADOConnection
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 792
    Top = 16
  end
end
