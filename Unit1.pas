unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, Unit2, Grids, DBGrids, DB, ADODB, StdCtrls, ShellAPI,
  ComCtrls;

type
  TMainForm = class(TForm)
    mm1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    ds1: TDataSource;
    dbgrd1: TDBGrid;
    tbl1: TADOTable;
    dbgrd2: TDBGrid;
    btn1: TButton;
    btn2: TButton;
    atncfldtbl1Group_id: TAutoIncField;
    wdstrngfldtbl1Group_numbers: TWideStringField;
    wdstrngfldtbl1Description: TWideStringField;
    btn3: TButton;
    N4: TMenuItem;
    btn7: TButton;
    PopupMenu1: TPopupMenu;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    tbl2: TADOTable;
    ds2: TDataSource;
    con1: TADOConnection;
    wdstrngfldtbl2Document_number: TWideStringField;
    wdstrngfldtbl2Document_name: TWideStringField;
    wdstrngfldtbl2Author: TWideStringField;
    wdstrngfldtbl2Created: TWideStringField;
    wdstrngfldtbl2Note: TWideStringField;
    atncfldtbl2id: TAutoIncField;
    intgrfldtbl2Group_id: TIntegerField;
    mfldtbl2File_path: TMemoField;
    mfldtbl2Model_path: TMemoField;
    N12: TMenuItem;
    N13: TMenuItem;
    stat1: TStatusBar;
    procedure N7Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure mfldtbl1DescriptionGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure btn3Click(Sender: TObject);
    procedure dbgrd1CellClick(Column: TColumn);
    procedure dbgrd2CellClick(Column: TColumn);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure N4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn7Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure dbgrd2MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure tbl1AfterScroll(DataSet: TDataSet);
  private
    procedure ConnectAsRead(FileName: string);
    procedure ConnectAsReadWrite(FileName: string);
    procedure SetTitleForModeName(ModeName: string);
    procedure SortDependentTable();
    procedure SortMainTable();
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;
  FileName: string;// ���� ���������� �����
  id1, id2: Integer; //��������� ������
  edit: Boolean; //����������� ������

implementation

uses Unit3, Unit4;

{$R *.dfm}

procedure TMainForm.N7Click(Sender: TObject);
begin
  MainForm.Close;
end;

procedure TMainForm.N5Click(Sender: TObject);
begin
  AboutForm.ShowModal;
end;


procedure TMainForm.N3Click(Sender: TObject);

var
  openDialog: TOpenDialog;
  iores: Boolean;
  HFileRes: HFILE;
begin
  //FileName:='';
  openDialog := TOpenDialog.Create(self);
  openDialog.InitialDir:=GetCurrentDir;
  openDialog.Filter := '���� ������ Microsoft Access (*.mdb)|*.mdb';
  if openDialog.Execute //���� ������ ����
    then
      begin
        if con1.Connected
          then con1.Connected:=false;  //��������� ADOConnection, ���� ���������
        FileName:=OpenDialog.FileName
      end
    else
      begin
        openDialog.Free;
        Exit;
      end;
  //http://stackoverflow.com/a/141376/662640
  if not FileExists(FileName)
    then
      Exit;
  HFileRes := CreateFile(PChar(FileName)
    ,GENERIC_READ or GENERIC_WRITE
    ,0
    ,nil
    ,OPEN_EXISTING
    ,FILE_ATTRIBUTE_NORMAL
    ,0);

  iores := (HFileRes = INVALID_HANDLE_VALUE);

  if not(iores)
    then
      CloseHandle(HFileRes);

  //http://stackoverflow.com/a/141376/662640
  if iores=False
    then
      begin
        ConnectAsReadWrite(FileName);
        tbl1.Active:=True;
        tbl2.Active:=True;
        btn1.Enabled:=True;
        btn2.Enabled:=True;
        btn3.Enabled:=True;
        PopupMenu1.Items[2].Enabled:=True;
        PopupMenu1.Items[3].Enabled:=True;
        N6.Enabled:=True;
        N12.Checked:=True;
        N4.Enabled:=True;
        SetTitleForModeName('����� ��������������');
        dbgrd1.Options:=dbgrd1.Options+[dgEditing];
      end
    else
      begin
        ConnectAsRead(FileName);
        tbl1.Active:=True;
        tbl2.Active:=True;
        btn1.Enabled:=False;
        btn2.Enabled:=False;
        btn3.Enabled:=False;
        PopupMenu1.Items[2].Enabled:=False;
        PopupMenu1.Items[3].Enabled:=False;
        N6.Enabled:=True;
        N13.Checked:=True;
        N4.Enabled:=True;
        SetTitleForModeName('����� ������');
        dbgrd1.Options:=dbgrd1.Options-[dgEditing];
        ShowMessage('���� ������ ������� � ������ ������!');
      end;
  SortMainTable();
  SortDependentTable();
  openDialog.Free;
end;

procedure TMainForm.btn1Click(Sender: TObject);
begin
  if not con1.Connected
    then Exit;
  tbl1.Append;
  dbgrd1.SetFocus;
end;

procedure TMainForm.btn2Click(Sender: TObject);

begin
  if not con1.Connected
    then Exit;
  if tbl1.RecordCount<>0
    then
      if MessageDlg('������� ������?', mtConfirmation, mbOKCancel, 0)=mrOK
        then
        tbl1.Delete;
  tbl1.Refresh;
  dbgrd1.Update;
  SortMainTable();
end;

procedure TMainForm.mfldtbl1DescriptionGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  Text:=Sender.AsString;
end;

procedure TMainForm.btn3Click(Sender: TObject);

begin
  Form3.Caption:='���������� ���������';
  if not con1.Connected
    then Exit;
  Form3.btn1.Caption:='��������';
  Form3.cbb1.Enabled:=True;
  Form3.ShowModal;
  SortDependentTable;
end;

procedure TMainForm.dbgrd1CellClick(Column: TColumn);
begin
//  if not con1.Connected
//    then Exit;
//  id1:=tbl1.FieldByName('Group_id').AsInteger;
//  SortDependentTable;
end;

procedure TMainForm.dbgrd2CellClick(Column: TColumn);
begin
  if not con1.Connected
    then Exit;
  id2:=tbl2.FieldByName('id').AsInteger;
end;

procedure TMainForm.dbgrd2MouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  if not con1.Connected
    then Exit;
  id2:=tbl2.FieldByName('id').AsInteger;
  SortDependentTable;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if tbl1.Modified
    then  tbl1.Post;
  try
    DeleteFile('ForReading')
  except
  end;
end;

procedure TMainForm.N4Click(Sender: TObject);

var
  f: textfile;

begin
  AssignFile(f, 'Settings.ini');
  Rewrite(f);
  Write(f, FileName);
  CloseFile(f);
end;

procedure TMainForm.FormShow(Sender: TObject);

var
  f: TextFile;
  iores: integer;

begin
  try
    DeleteFile('ForReading')
  except
  end;
  FileName:='';
  try
    if not FileExists('settings.ini')
      then Exit;
    AssignFile(f, 'settings.ini');
    Reset(f);
    if not Eof(f)
      then
        Readln(f, FileName);
    CloseFile(f);
    if not FileExists(FileName)
      then Exit;
    AssignFile(f, FileName);
    {$I-}
    Reset(f);
    {$I+}
    iores:=IOResult;
    if iores=0
    //if not FileOpen(FileName)
      then
        begin
          CloseFile(f);
          ConnectAsReadWrite(FileName);
          tbl1.Active:=True;
          tbl2.Active:=True;
          btn1.Enabled:=True;
          btn2.Enabled:=True;
          btn3.Enabled:=True;
          PopupMenu1.Items[2].Enabled:=True;
          PopupMenu1.Items[3].Enabled:=True;
          N6.Enabled:=True;
          N12.Checked:=True;
          N4.Enabled:=True;
          dbgrd1.Options:=dbgrd1.Options+[dgEditing];
          SetTitleForModeName('����� ��������������');
        end
      else
        begin
          ConnectAsRead(FileName);
          tbl1.Active:=True;
          tbl2.Active:=True;
          btn1.Enabled:=False;
          btn2.Enabled:=False;
          btn3.Enabled:=False;
          PopupMenu1.Items[2].Enabled:=False;
          PopupMenu1.Items[3].Enabled:=False;
          N6.Enabled:=True;
          N13.Checked:=True;
          N4.Enabled:=True;
          dbgrd1.Options:=dbgrd1.Options-[dgEditing];
          SetTitleForModeName('����� ������');
          ShowMessage('���� ������ ������� � ������ ������!');
        end;
  except
  end;
  SortMainTable();
  SortDependentTable();
  stat1.SimpleText:=FileName;
end;

procedure TMainForm.btn7Click(Sender: TObject);

var
  i: Integer;

begin
  if not con1.Connected
    then Exit;
  Form4.cbb1.ItemIndex:=-1;
  Form4.cbb1.Items.Clear;
  Form4.cbb1.Text:='';
  Form4.lbledt1.Text:='';
  for i:=Form4.strngrd1.FixedRows to Form4.strngrd1.RowCount-1 do
    Form4.strngrd1.Rows[i].Clear;
  Form4.ShowModal;
end;

procedure TMainForm.ConnectAsRead(FileName: string);
begin
  if not FileExists('ForReading') then
  begin
    CopyFile(PChar(FileName), PChar('ForReading'), True);
    FileSetAttr('ForReading', faHidden);
  end;
  con1.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+'ForReading'+';Mode=Read;';
  con1.Connected:=True;
end;

procedure TMainForm.ConnectAsReadWrite(FileName: string);
begin
  con1.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+FileName+';Mode=ReadWrite;';
  con1.Connected:=True;
end;

procedure TMainForm.N8Click(Sender: TObject);
begin
  if FileExists(tbl2.FieldByName('File_path').AsString)
    then
      ShellExecute(Handle, 'open', PChar(tbl2.FieldByName('File_path').AsString), nil, nil, SW_SHOW)
    else
      ShowMessage('���� ������� �� ������');
end;

procedure TMainForm.PopupMenu1Popup(Sender: TObject);
begin
  if not con1.Connected
    then Exit;
  if FileExists(tbl2.FieldByName('File_path').AsString)
    then PopupMenu1.Items.Items[0].Enabled:=True
    else PopupMenu1.Items.Items[0].Enabled:=False;
  if FileExists(tbl2.FieldByName('Model_path').AsString)
    then PopupMenu1.Items.Items[1].Enabled:=True
    else PopupMenu1.Items.Items[1].Enabled:=False;
  id1:=tbl1.FieldByName('Group_id').AsInteger;
  id2:=tbl2.FieldByName('id').AsInteger;
end;


procedure TMainForm.SetTitleForModeName(ModeName: string);
begin
  MainForm.Caption:='�������� ��� ������ ��'+' ['+ModeName+']';
end;

procedure TMainForm.SortDependentTable;
begin
  tbl2.Active:=False;
  tbl2.Active:=True;
  tbl2.Sort:='Document_name';
end;

procedure TMainForm.SortMainTable;
begin
  tbl1.Active:=False;
  tbl1.Active:=True;
  tbl1.Sort:='Group_numbers';
end;

procedure TMainForm.tbl1AfterScroll(DataSet: TDataSet);
begin
  if not con1.Connected
    then Exit;
  //id1:=tbl1.FieldByName('Group_id').AsInteger;
  SortDependentTable;
end;

procedure TMainForm.N10Click(Sender: TObject);
begin
  Form3.Caption:='��������� ���������';
  edit:=True;
  tbl2.First;
  while (tbl2.FieldByName('id').AsInteger<>id2) and (not tbl2.Eof)
    do
      tbl2.Next;
  tbl2.Edit;
  Form3.lbled1.Text:=tbl2.FieldByName('Document_number').AsString;
  Form3.lbled2.Text:=tbl2.FieldByName('Document_name').AsString;
  Form3.lbled3.Text:=tbl2.FieldByName('Author').AsString;
  Form3.lbled4.Text:=tbl2.FieldByName('File_path').AsString;
  Form3.lbled6.Text:=tbl2.FieldByName('Model_path').AsString;
  Form3.lbled5.Text:=tbl2.FieldByName('Note').AsString;
  id1:=tbl2.FieldByName('Group_id').AsInteger;
  Form3.btn1.Caption:='��������';
  //MainForm.tbl2.FieldByName('Created'):=//���� ��������
  Form3.ShowModal;
end;

procedure TMainForm.N11Click(Sender: TObject);
begin
  if tbl2.RecordCount<>0
    then
      if MessageDlg('������� ��������?', mtConfirmation, mbOKCancel, 0)=mrOK
        then
          tbl2.Delete;
  tbl2.Refresh;
  dbgrd2.Update;
end;

procedure TMainForm.N9Click(Sender: TObject);
begin
  if FileExists(tbl2.FieldByName('Model_path').AsString)
    then
      ShellExecute(Handle, 'open', PChar(tbl2.FieldByName('Model_path').AsString), nil, nil, SW_SHOW)
    else
      ShowMessage('���� ������ �� ������');
end;

procedure TMainForm.N12Click(Sender: TObject);

var
  iores: Boolean;
  HFileRes: HFILE;

begin

  con1.Connected:=False;
  //if FileExists(ChangeFileExt(FileName, '*.ldb'))
   // then DeleteFile(ChangeFileExt(FileName, '*.ldb'));
  HFileRes := CreateFile(PChar(FileName)
    ,GENERIC_READ or GENERIC_WRITE
    ,0
    ,nil
    ,OPEN_EXISTING
    ,FILE_ATTRIBUTE_NORMAL
    ,0);

  iores := (HFileRes = INVALID_HANDLE_VALUE);
  CloseHandle(HFileRes);

  if not iores
    then
      begin
        N12.Checked:=True;
        //CloseHandle(HFileRes);
        ConnectAsReadWrite(FileName);
        tbl1.Active:=True;
        tbl2.Active:=True;
        btn1.Enabled:=True;
        btn2.Enabled:=True;
        btn3.Enabled:=True;
        PopupMenu1.Items[2].Enabled:=True;
        PopupMenu1.Items[3].Enabled:=True;
        SetTitleForModeName('����� ��������������');
        dbgrd1.Options:=dbgrd1.Options+[dgEditing];
      end
    else
      begin
        ConnectAsRead(FileName);
        tbl1.Active:=True;
        tbl2.Active:=True;
        btn1.Enabled:=False;
        btn2.Enabled:=False;
        btn3.Enabled:=False;
        PopupMenu1.Items[2].Enabled:=False;
        PopupMenu1.Items[3].Enabled:=False;
        N6.Enabled:=True;
        N13.Checked:=True;
        dbgrd1.Options:=dbgrd1.Options-[dgEditing];
        SetTitleForModeName('����� ������');
        ShowMessage('���� ������ ��� ������� � ������ ��������������!');
      end;
  SortMainTable();
  SortDependentTable();
end;

procedure TMainForm.N13Click(Sender: TObject);

begin
  N13.Checked:=True;
  con1.Connected:=False;
  ConnectAsRead(FileName);
  tbl1.Active:=True;
  tbl2.Active:=True;
  SortDependentTable;
  btn1.Enabled:=False;
  btn2.Enabled:=False;
  btn3.Enabled:=False;
  PopupMenu1.Items[2].Enabled:=False;
  PopupMenu1.Items[3].Enabled:=False;
  N6.Enabled:=True;
  N13.Checked:=True;
  dbgrd1.Options:=dbgrd1.Options-[dgEditing];
  SetTitleForModeName('����� ������');
  SortMainTable();
  SortDependentTable();
end;

end.
