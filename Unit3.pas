unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TForm3 = class(TForm)
    btn1: TButton;
    btn2: TButton;
    lbled1: TLabeledEdit;
    lbled2: TLabeledEdit;
    lbled3: TLabeledEdit;
    lbled5: TLabeledEdit;
    cbb1: TComboBox;
    lbl1: TLabel;
    lbled4: TLabeledEdit;
    btn3: TButton;
    lbled6: TLabeledEdit;
    Button1: TButton;
    procedure btn2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses Unit1;

{$R *.dfm}

procedure TForm3.btn2Click(Sender: TObject);
begin
  Form3.Close;
  edit:=False;
  lbled1.Text:='';
  lbled2.Text:='';
  lbled3.Text:='';
  lbled4.Text:='';
  lbled5.Text:='';
  cbb1.ItemIndex:=-1;
end;

procedure TForm3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  cbb1.Items.Clear;
  edit:=False;
  lbled1.Text:='';
  lbled2.Text:='';
  lbled3.Text:='';
  lbled4.Text:='';
  lbled5.Text:='';
  lbled6.Text:='';
  cbb1.ItemIndex:=-1;
end;

procedure TForm3.FormShow(Sender: TObject);

var
  i: Integer;
begin
  MainForm.tbl1.First;
  for i:=0 to (MainForm.tbl1.RecordCount-1)
    do
      begin
        //Form3.cbb1.Items.Add(MainForm.tbl1.Fields.Fields[i].AsString);
        Form3.cbb1.Items.Add(MainForm.tbl1.FieldByName('Group_numbers').AsString);
        MainForm.tbl1.Next;
      end;
  Form3.cbb1.Update;
  MainForm.tbl1.First;
  i:=0;
  while (MainForm.tbl1.FieldByName('Group_id').AsInteger<>id1) and (not MainForm.tbl1.Eof)
    do
      begin
        MainForm.tbl1.Next;
        Inc(i);
      end;
  if edit
    then
      begin
        cbb1.ItemIndex:=i;
        cbb1.Enabled:=False;
      end;
  MainForm.tbl2.First;
  while (MainForm.tbl2.FieldByName('id').AsInteger<>id2) and (not MainForm.tbl2.Eof)
    do
      MainForm.tbl2.Next;
end;

procedure TForm3.btn1Click(Sender: TObject);

var
  i: Integer;

begin
  if cbb1.ItemIndex=-1
    then
      begin
        ShowMessage('�� ������� ������');
        exit;
      end;
  if (lbled1.Text='') or (lbled2.Text='') or (lbled3.Text='') //or (lbled4.Text='')
    then
      begin
        ShowMessage('������� �����, ��������, ������� ������');
        Exit;
      end;
  MainForm.tbl1.First;
  while (MainForm.tbl1.FieldByName('Group_numbers').AsString<>cbb1.Items.Strings[cbb1.ItemIndex]) and (not MainForm.tbl1.Eof)
    do
      MainForm.tbl1.Next;
  i:=MainForm.tbl1.FieldByName('Group_id').AsInteger;
  if not edit
    then MainForm.tbl2.Append;
  while (MainForm.tbl2.FieldByName('id').AsInteger<>id2) and (not MainForm.tbl2.Eof)
    do
      MainForm.tbl2.Next;
  MainForm.tbl2.Edit;
  MainForm.tbl2.FieldByName('Document_number').AsString:=lbled1.Text;
  MainForm.tbl2.FieldByName('Document_name').AsString:=lbled2.Text;
  MainForm.tbl2.FieldByName('Author').AsString:=lbled3.Text;
  MainForm.tbl2.FieldByName('File_path').AsString:=lbled4.Text;
  MainForm.tbl2.FieldByName('Model_path').AsString:=lbled6.Text;
  try
    MainForm.tbl2.FieldByName('Created').AsDateTime:=FileDateToDateTime(FileAge(MainForm.tbl2.FieldByName('File_path').AsString));
  except
    ShowMessage('���������� ������� ���� �������');
  end;

  if not FileExists(MainForm.tbl2.FieldByName('Model_path').AsString)
    then
      ShowMessage('���������� ������� ���� ������');

  MainForm.tbl2.FieldByName('Group_id').AsInteger:=i;
  MainForm.tbl2.FieldByName('Note').AsString:=lbled5.Text;
  MainForm.tbl2.Post;
  MainForm.tbl2.Refresh;
  MainForm.dbgrd2.Refresh;
  lbled1.Text:='';
  lbled2.Text:='';
  lbled3.Text:='';
  lbled4.Text:='';
  lbled5.Text:='';
  cbb1.ItemIndex:=-1;
  edit:=False;
  Form3.Close;

end;

procedure TForm3.btn3Click(Sender: TObject);

var
  openDialog: TOpenDialog;

begin
  openDialog := TOpenDialog.Create(self);
  openDialog.InitialDir:=GetCurrentDir;
  openDialog.Filter := '��������� ����� (*.frw; *.cdw)|*.cdw;*.frw';
  if openDialog.Execute //���� ������ ����
    then
      lbled4.Text:=OpenDialog.FileName;
  openDialog.Free;
end;

procedure TForm3.Button1Click(Sender: TObject);

var
  openDialog: TOpenDialog;
  
begin
  openDialog := TOpenDialog.Create(self);
  openDialog.InitialDir:=GetCurrentDir;
  openDialog.Filter := '��������� ����� (*.m3d; *.a3d)|*.m3d;*.a3d';
  if openDialog.Execute //���� ������ ����
    then
      lbled6.Text:=OpenDialog.FileName;
  openDialog.Free;
end;

end.
