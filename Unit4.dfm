object Form4: TForm4
  Left = 1243
  Top = 250
  BorderStyle = bsDialog
  Caption = #1055#1086#1080#1089#1082' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  ClientHeight = 418
  ClientWidth = 655
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 10
    Top = 9
    Width = 36
    Height = 13
    Caption = #1043#1088#1091#1087#1087#1072
  end
  object strngrd1: TStringGrid
    Left = 10
    Top = 57
    Width = 636
    Height = 350
    BorderStyle = bsNone
    DefaultRowHeight = 18
    FixedCols = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
    TabOrder = 0
    ColWidths = (
      102
      102
      64
      64
      64)
  end
  object cbb1: TComboBox
    Left = 10
    Top = 25
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
  end
  object lbledt1: TLabeledEdit
    Left = 171
    Top = 25
    Width = 185
    Height = 21
    EditLabel.Width = 93
    EditLabel.Height = 13
    EditLabel.Caption = #1055#1086#1080#1089#1082#1086#1074#1099#1081' '#1079#1072#1087#1088#1086#1089
    TabOrder = 2
  end
  object btn1: TButton
    Left = 363
    Top = 21
    Width = 75
    Height = 25
    Caption = #1053#1072#1081#1090#1080'...'
    TabOrder = 3
    OnClick = btn1Click
  end
  object btn2: TButton
    Left = 572
    Top = 21
    Width = 75
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    TabOrder = 4
    OnClick = btn2Click
  end
  object btn3: TButton
    Left = 447
    Top = 21
    Width = 75
    Height = 25
    Caption = #1042#1099#1073#1088#1072#1090#1100
    TabOrder = 5
    OnClick = btn3Click
  end
end
