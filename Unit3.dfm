object Form3: TForm3
  Left = 473
  Top = 313
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  ClientHeight = 300
  ClientWidth = 334
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 10
    Top = 13
    Width = 100
    Height = 13
    Caption = #1043#1088#1091#1087#1087#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
  end
  object btn1: TButton
    Left = 250
    Top = 16
    Width = 75
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 0
    OnClick = btn1Click
  end
  object btn2: TButton
    Left = 250
    Top = 48
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 1
    OnClick = btn2Click
  end
  object lbled1: TLabeledEdit
    Left = 10
    Top = 70
    Width = 200
    Height = 21
    EditLabel.Width = 92
    EditLabel.Height = 13
    EditLabel.Caption = ' '#1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    TabOrder = 2
  end
  object lbled2: TLabeledEdit
    Left = 10
    Top = 110
    Width = 200
    Height = 21
    EditLabel.Width = 106
    EditLabel.Height = 13
    EditLabel.Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    TabOrder = 3
  end
  object lbled3: TLabeledEdit
    Left = 10
    Top = 150
    Width = 200
    Height = 21
    EditLabel.Width = 86
    EditLabel.Height = 13
    EditLabel.Caption = #1040#1074#1090#1086#1088' ('#1092#1072#1084#1080#1083#1080#1103')'
    TabOrder = 4
  end
  object lbled5: TLabeledEdit
    Left = 10
    Top = 270
    Width = 280
    Height = 21
    EditLabel.Width = 61
    EditLabel.Height = 13
    EditLabel.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
    TabOrder = 5
  end
  object cbb1: TComboBox
    Left = 10
    Top = 30
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 6
  end
  object lbled4: TLabeledEdit
    Left = 10
    Top = 190
    Width = 280
    Height = 21
    EditLabel.Width = 206
    EditLabel.Height = 13
    EditLabel.Caption = #1056#1072#1089#1087#1086#1083#1086#1078#1077#1085#1080#1077' '#1095#1077#1088#1090#1077#1078#1072' '#1076#1077#1090#1072#1083#1080' ('#1089#1073#1086#1088#1082#1080')'
    TabOrder = 7
  end
  object btn3: TButton
    Left = 300
    Top = 186
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 8
    OnClick = btn3Click
  end
  object lbled6: TLabeledEdit
    Left = 10
    Top = 230
    Width = 280
    Height = 21
    EditLabel.Width = 199
    EditLabel.Height = 13
    EditLabel.Caption = #1056#1072#1089#1087#1086#1083#1086#1078#1077#1085#1080#1077' '#1084#1086#1076#1077#1083#1080' '#1076#1077#1090#1072#1083#1080' ('#1089#1073#1086#1088#1082#1080')'
    TabOrder = 9
  end
  object Button1: TButton
    Left = 300
    Top = 226
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 10
    OnClick = Button1Click
  end
end
